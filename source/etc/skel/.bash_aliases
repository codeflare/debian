case $- in
  *i*) ;;
    *) return;;
esac

export HISTFILESIZE=1000
export HISTCONTROL=ignoredups

shopt -s histappend

bind '"\e[1;5C": forward-word'
bind '"\e[1;5D": backward-word'

alias ls="LC_ALL=C ls -aG --color=auto"
alias ll="ls -l"
alias cl="clear"
alias xu="sudo su -"

function rnd {
  echo "(${1:-32}) `cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${1:-32} | head -n 1`"
}

function bak {
  cp -r "$1"{,.bak-`date +%Y%m%d%H%M%S`}
}

function cdl {
  cd "$@" && ls
}

function mcd {
  mkdir -p "$@" && cd "$_";
}

function ext {
  if [ -f "$1" ]; then
    case "${1,,}" in
      *.tar.bz2) tar -jxvf "$1"                    ;;
      *.tar.gz)  tar -zxvf "$1"                    ;;
      *.bz2)     bunzip2 "$1"                      ;;
      *.dmg)     hdiutil mount "$1"                ;;
      *.gz)      gunzip "$1"                       ;;
      *.tar)     tar -xvf "$1"                     ;;
      *.tbz2)    tar -jxvf "$1"                    ;;
      *.tgz)     tar -zxvf "$1"                    ;;
      *.zip)     unzip "$1"                        ;;
      *.pax)     cat "$1" | pax -r                 ;;
      *.pax.z)   uncompress "$1" --stdout | pax -r ;;
      *.z)       uncompress "$1"                   ;;
      *)         echo "ext: $1: Unknown method"    ;;
    esac
  else
    echo "ext: $1: No such file or directory"
  fi
}

if [[ "`whoami`" == "root" ]]; then
  PS1_USER_COLOR="\e[1;31m";
else
  PS1_USER_COLOR="\e[1;33m";
fi

PS1="\n\[${PS1_USER_COLOR}\]\u\[\e[0;m\] at \[\e[1;34m\]\H\[\e[0;m\] in \[\e[1;32m\]\w\[\e[0;m\] \n\\$ \[\e[m\]"
